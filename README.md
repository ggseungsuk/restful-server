# Name

###### restful-server

# Description

RESTful API를 지원하는 데모 서버

### Development environment
	tool : eclipse-4.12.0
	java : jdk-1.8.0_161
	DB : mongo-3.2.0-26
	framework : spring-webflux
	  
### Properties
open [application.yml](./src/main/resources/application.yml)	

### Eclipse run
	Project > Right Click > Run As > Maven Build... > Goals: spring-boot:run > [Run] Click

### Install
	Project > Right Click > Run As > Maven Install
  
### Run
open <a href="./target/restful-server" target="_blank">Installed Directory</a>

window

```bash
start.cmd
cmd window close
```
linux

```bash
start.sh
stop.sh
```
	
## Manual for User. 
[Restful-Server-User-Manual.docx](manual\Restful-Server-User-Manual.docx)  
  
