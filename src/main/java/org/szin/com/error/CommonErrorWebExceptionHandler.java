package org.szin.com.error;

import static org.springframework.web.reactive.function.server.RequestPredicates.all;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.AbstractErrorWebExceptionHandler;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.HttpMessageWriter;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import reactor.core.publisher.Mono;

@Component
@Order(-2)
public class CommonErrorWebExceptionHandler extends AbstractErrorWebExceptionHandler{

	@Autowired 
	ServerCodecConfigurer serverCodecConfigurer;
	
	@PostConstruct
	public void serverResponseResultHandler() {
		setMessageWriters(serverCodecConfigurer.getWriters());
	}
	

    public CommonErrorWebExceptionHandler(ErrorAttributes errorAttributes, ResourceProperties resourceProperties, ApplicationContext applicationContext, ServerCodecConfigurer configurer) {
        super(errorAttributes, resourceProperties, applicationContext);
        this.setMessageWriters(configurer.getWriters());
    }

    @Override
    protected RouterFunction<ServerResponse> getRoutingFunction(
      ErrorAttributes errorAttributes) {

		return route(all(), this::renderErrorResponse);
    }

	protected Mono<ServerResponse> renderErrorResponse(ServerRequest request) {
		Map<String, Object> error = getErrorAttributes(request, false);
				
		MediaType contentType = MediaType.APPLICATION_JSON_UTF8;
		
		List<MediaType> accepts = request.headers().accept();
		
		if(accepts != null && accepts.size() > 0 && !accepts.contains(MediaType.ALL)) {			
			for(MediaType type : accepts) {
				if(type.isCompatibleWith(MediaType.APPLICATION_JSON)) break;
				else if(type.isCompatibleWith(MediaType.APPLICATION_XML)) { contentType = MediaType.APPLICATION_XML; }
			}			
		}

		return ServerResponse.status(getHttpStatus(error)).contentType(contentType)
				.body(BodyInserters.fromObject(""));
	}
	
	protected HttpStatus getHttpStatus(Map<String, Object> errorAttributes) {
		int statusCode = (int) errorAttributes.get("status");
		return HttpStatus.valueOf(statusCode);
	}
}
