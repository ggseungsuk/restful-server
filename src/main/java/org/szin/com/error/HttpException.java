package org.szin.com.error;

import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;

import org.szin.util.StringUtil;

public class HttpException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4650999593284308727L;
	
	private HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
	private String cd = "500";
	private String field = "";
	private String[] args;

		
	public HttpException(){
	}
	
	public HttpException(String cd){
		super(cd);
		this.cd = cd;
		this.status = HttpStatus.BAD_GATEWAY;
	}

	public HttpException(String cd, String args){
		super(cd);
		this.cd = cd;
		this.args = new String[]{args};
	}

	public HttpException(String cd, String[] args){
		super(cd);
		this.cd = cd;
		this.args = args;
	}

	public HttpException(String cd, HttpStatus status){
		super(cd);
		this.cd = cd;
		this.status = status;
	}

	public HttpException(String cd, String[] args, HttpStatus status){
		super(cd);
		this.cd = cd;
		this.args = args;
		this.status = status;
	}
	
	public HttpException(String cd, String[] args, String field){
		super(cd);
		this.cd = cd;
		this.args = args;
		this.field = field;
	}
	
	public HttpException(Throwable cause){
		super(cause);
	}

	
	public HttpException(Throwable cause, HttpStatus status){
		super(cause);
		this.status = status;
	}
		

	public void setCd(String cd) {
		this.cd = cd;
	}
	
	public String getCd() {
		return cd;
	}

	public String[] getArgs() {
		return args;
	}

	public void setArgs(String[] args) {
		this.args = args;
	}	
	
	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getMsg(){
		try{
			return StringUtil.code2message("error." + this.cd, getArgs());
		}catch(NoSuchMessageException e){
			//LogUtil.error(e);
			return StringUtil.code2message("error.500", null) + "\n" +  getMessage();
		}
	}	
}
