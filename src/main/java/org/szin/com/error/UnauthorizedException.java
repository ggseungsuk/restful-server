package org.szin.com.error;

import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;

import org.szin.util.StringUtil;

public class UnauthorizedException extends HttpException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4650999593284308727L;
	
	private HttpStatus status = HttpStatus.UNAUTHORIZED;
	private String cd = "401";
	private String field = "";
	private String[] args;

		
	public UnauthorizedException(){
	}
	
	public UnauthorizedException(String cd){
		this.cd = cd;
		this.status = HttpStatus.UNAUTHORIZED;
	}

	public UnauthorizedException(String cd, String args){
		this.cd = cd;
		this.args = new String[]{args};
	}

	public UnauthorizedException(String cd, String[] args){
		this.cd = cd;
		this.args = args;
	}

	public UnauthorizedException(String cd, HttpStatus status){
		this.cd = cd;
		this.status = status;
	}

	public UnauthorizedException(String cd, String[] args, HttpStatus status){
		this.cd = cd;
		this.args = args;
		this.status = status;
	}
	
	public UnauthorizedException(String cd, String[] args, String field){
		this.cd = cd;
		this.args = args;
		this.field = field;
	}
	
	public UnauthorizedException(Throwable cause){
		super(cause);
	}

	
	public UnauthorizedException(Throwable cause, HttpStatus status){
		super(cause);
		this.status = status;
	}
		

	public void setCd(String cd) {
		this.cd = cd;
	}
	
	public String getCd() {
		return cd;
	}

	public String[] getArgs() {
		return args;
	}

	public void setArgs(String[] args) {
		this.args = args;
	}	
	
	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getMsg(){
		try{
			return StringUtil.code2message("error." + this.cd, getArgs());
		}catch(NoSuchMessageException e){
			//LogUtil.error(e);
			return StringUtil.code2message("error.500", null) + "\n" +  getMessage();
		}
	}	
}
