package org.szin.com.error;

import java.util.Map;

import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;

import org.szin.com.server.wrapper.ServerHttpResponseWrapper;
import org.szin.com.server.wrapper.ServerWebExchangeWrapper;
import org.szin.com.vo.ErrorVo;

@Component
public class HttpErrorAttributes implements ErrorAttributes {
	private static final String ERROR_ATTRIBUTE = HttpErrorAttributes.class.getName() + ".ERROR";

	@Override
	public Map<String, Object> getErrorAttributes(ServerRequest request, boolean includeStackTrace) {
		Throwable error = (Throwable) request.attribute(ERROR_ATTRIBUTE).get();		
		if(error == null) error = new HttpException();
		String msg = error.getMessage();
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		if(error instanceof ResponseStatusException)
			status = ((ResponseStatusException)error).getStatus();
		else if(error instanceof HttpException) 
			status = ((HttpException)error).getStatus();
		
		ServerHttpResponse response = request.exchange().getResponse();
		Map<String, Object> errorAttributes = new ErrorVo<String, Object>();
		errorAttributes.put("status", status.value());
		response.getHeaders().add("timestamp", String.valueOf(System.currentTimeMillis()));
		response.getHeaders().add("rsm", msg);
	
		return errorAttributes;
	}

	@Override
	public Throwable getError(ServerRequest request) {
		return (Throwable) request.attribute(ERROR_ATTRIBUTE)
				.orElseThrow(() -> new IllegalStateException("Missing exception attribute in ServerWebExchange"));
	}
	
	@Override
	public void storeErrorInformation(Throwable error, ServerWebExchange exchange) {
		exchange.getAttributes().putIfAbsent(ERROR_ATTRIBUTE, error);
	}
}
