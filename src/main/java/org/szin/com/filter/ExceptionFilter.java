package org.szin.com.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import org.szin.com.error.HttpException;
import org.szin.com.server.wrapper.ServerWebExchangeWrapper;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@Order(2)
public class ExceptionFilter implements WebFilter {

	
	
	private final static Logger logger = LoggerFactory.getLogger(ExceptionFilter.class);

	@Override
	public Mono<Void> filter(ServerWebExchange serverWebExchange, WebFilterChain webFilterChain) {
		if (!(serverWebExchange instanceof ServerWebExchangeWrapper)) {
			serverWebExchange = new ServerWebExchangeWrapper(serverWebExchange);
		}
		
		final ServerWebExchangeWrapper serverWebExchangeWrapper = (ServerWebExchangeWrapper)serverWebExchange;

		return webFilterChain.filter(serverWebExchange).onErrorResume((c) -> {return handleError(serverWebExchangeWrapper, c);});
	}
	
	
	private Mono<Void> handleError(ServerWebExchange serverWebExchange, Throwable error){	
		if(error == null) error = new HttpException();
		String msg = error.getMessage();
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		if(error instanceof ResponseStatusException)
			status = ((ResponseStatusException)error).getStatus();
		else if(error instanceof HttpException) 
			status = ((HttpException)error).getStatus();
		
		ServerHttpResponse response = serverWebExchange.getResponse();
		response.setStatusCode(status);
		response.getHeaders().add("timestamp", String.valueOf(System.currentTimeMillis()));
		response.getHeaders().add("rsm", msg);
		
		
		return Mono.empty();
	}
	
}