package org.szin.com.filter;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import org.szin.com.server.wrapper.ServerHttpRequestWrapper;
import org.szin.com.server.wrapper.ServerHttpResponseWrapper;
import org.szin.com.server.wrapper.ServerWebExchangeWrapper;

import reactor.core.publisher.Mono;

@Component
@Order(3)
public class ReponseForwardFilter implements WebFilter {

	private final static Logger logger = LoggerFactory.getLogger(ReponseForwardFilter.class);
	
	@Value("${http.header.response-forward.source-key:response-forward-source}")
	private String responseForwardSource;
	
	@Value("${http.header.response-forward.target-key:response-forward-target}")
	private String responseForwardTarget;

	@Value("${http.header.response-forward.status-key:response-forward-status}")
	private String responseForwardStatus;

	@Override
	public Mono<Void> filter(ServerWebExchange serverWebExchange, WebFilterChain webFilterChain) {
		
		final String forwardTarget = serverWebExchange.getRequest().getHeaders().toSingleValueMap().getOrDefault(responseForwardTarget, "");
		// forward-url이 없으면 통과
		if("".equals(forwardTarget.trim())) {
			return webFilterChain.filter(serverWebExchange);
		}
		
		if (!(serverWebExchange instanceof ServerWebExchangeWrapper)) {
			serverWebExchange = new ServerWebExchangeWrapper(serverWebExchange);
		}
		
		final ServerWebExchangeWrapper serverWebExchangeWrapper = (ServerWebExchangeWrapper)serverWebExchange;
		serverWebExchangeWrapper.enableResponseCache();
		return webFilterChain.filter(serverWebExchange).doOnSuccess(s -> {handleForward(forwardTarget, serverWebExchangeWrapper);});
		
	}
	
	@Autowired
	WebClient webClient;
	
	private void handleForward(String forwardTarget, ServerWebExchangeWrapper serverWebExchangeWrapper){

		ServerHttpResponseWrapper response = (ServerHttpResponseWrapper)serverWebExchangeWrapper.getResponse();
		response.doOnCompleteBodyCache(()->{
	
			try {
				ServerHttpRequestWrapper request = (ServerHttpRequestWrapper)serverWebExchangeWrapper.getRequest();
				
				WebClient.RequestBodySpec spec = webClient.method(request.getMethod()).uri(forwardTarget);
					
				if(!"0".equals(response.getHeaders().toSingleValueMap().getOrDefault("Content-Length", "0"))) {
					spec.body(BodyInserters.fromPublisher(Mono.just(response.getCachedBody()), String.class));
					//spec.syncBody(response.getCachedBody()) ;
					//spec.body(BodyInserters.fromPublisher(Mono.form(body), DataBuffer.class));
				}
				
				Set<String> headerKeys = response.getHeaders().keySet();
				for(String key : headerKeys) {
					List<String> headerValues = response.getHeaders().getValuesAsList(key);
					spec.header(key, headerValues.toArray(new String[headerValues.size()]));
				}			
				
				spec.header(responseForwardSource, request.getHeaders().getOrDefault(responseForwardSource, Arrays.asList(request.getRemoteAddress().toString())).get(0));
	
				spec.exchange().doAfterSuccessOrError((s , t) -> {
					//response.getHeaders().add(responseForwardStatus, String.valueOf(s.statusCode().value()));
				}).subscribe();
				
			}catch(Exception e) {
				e.printStackTrace();
			}		
		});
	}
}