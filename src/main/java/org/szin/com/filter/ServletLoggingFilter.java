package org.szin.com.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import org.szin.com.server.wrapper.ServerHttpRequestWrapper;
import org.szin.com.server.wrapper.ServerHttpResponseWrapper;
import org.szin.com.server.wrapper.ServerWebExchangeWrapper;
import org.szin.util.StopPointUtil;

import reactor.core.publisher.Mono;

@Component
@Order(1)
public class ServletLoggingFilter implements WebFilter {

	private final static Logger logger = LoggerFactory.getLogger(ServletLoggingFilter.class);	
	private final static String FILTER_SERVER_LOGGING_START_MS = "__stm";

	@Value("${http.header.trace-logging-key:trace-logging}")
	private String traceLoggingKey;	

	
	public interface TraceChecker{
		public boolean isTrace(ServerWebExchange serverWebExchange);
	}
	
	private TraceChecker traceChecker = new TraceChecker() {
		public boolean isTrace(ServerWebExchange serverWebExchange) {
			return serverWebExchange.getRequest().getHeaders().toSingleValueMap().getOrDefault(traceLoggingKey, "false").equalsIgnoreCase("true");
		}
	};
	
	public void setTraceChecker(TraceChecker traceChecker) {
		this.traceChecker = traceChecker;
	}
	
	@Override
	public Mono<Void> filter(ServerWebExchange serverWebExchange, WebFilterChain webFilterChain) {
		
		if(StopPointUtil.isStopPoint(serverWebExchange, "ServletLoggingFilter")) return Mono.empty();
		
		// info 레벨 이하로만 로그를 남긴다.
		if(!logger.isInfoEnabled() && !traceChecker.isTrace(serverWebExchange)){
			return webFilterChain.filter(serverWebExchange);
		}
		
		if(!(serverWebExchange instanceof ServerWebExchangeWrapper)) {
			serverWebExchange = new ServerWebExchangeWrapper(serverWebExchange);
		}

		ServerWebExchangeWrapper serverWebExchangeWrapper = (ServerWebExchangeWrapper)serverWebExchange;
		
		initLogging(serverWebExchangeWrapper);
		loggingRequest(serverWebExchangeWrapper);
				
		return webFilterChain.filter(serverWebExchangeWrapper).doOnSuccess((b)->{
			loggingResponse(serverWebExchangeWrapper);
		}).doOnError(cause->{logger.error(cause.getMessage(), cause);});
	}
	
	private void initLogging(ServerWebExchange serverWebExchange) {
		if(logger.isDebugEnabled() || traceChecker.isTrace(serverWebExchange)) {
			((ServerWebExchangeWrapper)serverWebExchange).enableCache();
		}		
		serverWebExchange.getAttributes().put(FILTER_SERVER_LOGGING_START_MS, System.currentTimeMillis());
	}
	
	/**
	 * Request Logging
	 * @param serverWebExchange
	 */
	private void loggingRequest(final ServerWebExchange serverWebExchange) {
		
		try {

			if(logger.isInfoEnabled() || traceChecker.isTrace(serverWebExchange)) {
				ServerHttpRequest request = serverWebExchange.getRequest();
		        final StringBuilder logMessage = new StringBuilder()
		        		   .append("(IN )Request: ")
		        		   .append(request.getRemoteAddress()).append(" -> [")
		        		   .append(request.getMethod()).append("] ").append(request.getURI().getPath());
		        	        
				if(logger.isDebugEnabled() || traceChecker.isTrace(serverWebExchange)){
					final ServerHttpRequestWrapper cachedRequest = (ServerHttpRequestWrapper)request;
	
					logMessage.append("\n    Headers: ").append(cachedRequest.getHeaders().toSingleValueMap().toString());
					logMessage.append("\n    Parameters: ").append(cachedRequest.getQueryParams().toString());
					
					if("0".equals(cachedRequest.getHeaders().toSingleValueMap().getOrDefault("Content-Length", "0"))) {
						logger.debug(logMessage.toString());
					}else {
						cachedRequest.doOnCompleteBodyCache(()->{
							logMessage.append("\n    Body: ").append(cachedRequest.getCachedBody());
							logMessage.append("\n");
							logger.debug(logMessage.toString());
						});
					}
									
				}else if(logger.isInfoEnabled()) {
					logger.info(logMessage.toString());
				}
			}
		}catch(Exception e) {
			try {
				logger.warn(e.getMessage());
			}catch(Exception e2) {}
		}
	}

	private void loggingResponse(ServerWebExchange serverWebExchange) {

		try {
			if(logger.isInfoEnabled() || traceChecker.isTrace(serverWebExchange)) {
				ServerHttpRequest request = serverWebExchange.getRequest();
				ServerHttpResponse response = serverWebExchange.getResponse();
	
		        final StringBuilder logMessage = new StringBuilder()
		        	   .append("(OUT)Response: ").append("[")
		        	   .append(request.getMethod()).append("] ").append(request.getURI().getPath()).append(" -> ")
		        	   .append(request.getRemoteAddress())
		        	   .append("(status: ").append(response.getStatusCode())
		        	   .append(", ").append(System.currentTimeMillis()-(long)serverWebExchange.getAttribute(FILTER_SERVER_LOGGING_START_MS)).append("ms)");
	    		
		        if(logger.isDebugEnabled() || traceChecker.isTrace(serverWebExchange)){
					final ServerHttpResponseWrapper cachedResponse = (ServerHttpResponseWrapper)response;
					logMessage.append("\n    Headers: ").append(response.getHeaders().toSingleValueMap().toString());
	
					if("0".equals(cachedResponse.getHeaders().toSingleValueMap().getOrDefault("Content-Length", "0"))) {
						logger.debug(logMessage.toString());
					}else {
						cachedResponse.doOnCompleteBodyCache(()->{
							logMessage.append("\n    Body: ").append(cachedResponse.getCachedBody());
							logger.debug(logMessage.toString());
							
						});
					}			
					
	    		}else if(logger.isInfoEnabled()) {
					logger.info(logMessage.toString());
	    		}
			}
		}catch(Exception e) {
			try {
				logger.warn(e.getMessage());
			}catch(Exception e2) {}
		}
	}
}