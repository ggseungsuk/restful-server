package org.szin.com.config;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.netty.NettyReactiveWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.codec.Decoder;
import org.springframework.core.codec.Encoder;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.function.client.WebClient;

import org.szin.util.OrgJson2XmlDecoder;
import org.szin.util.OrgJsonXmlEncoder;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import reactor.netty.http.client.HttpClient;
import reactor.netty.tcp.TcpClient;

@Configuration
public class WebConfig implements WebFluxConfigurer {

	@Value("${server.connectiontimeout:10000}")
	int connectionTimeout;

	@Value("${server.writetimeout:10000}")
	int writetimeout;
	
	@Value("${server.readtimeout:10000}")
	int readtimeout;

	class NettyTimeoutCustomizer implements WebServerFactoryCustomizer<NettyReactiveWebServerFactory> {

		@Override
		public void customize(NettyReactiveWebServerFactory factory) {
			factory.addServerCustomizers(server -> server.tcpConfiguration(
					tcp -> tcp.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, connectionTimeout).doOnConnection(
							connection -> connection.addHandlerLast(new WriteTimeoutHandler(writetimeout)))));
		}
	}

	@Override
	public void addCorsMappings(CorsRegistry corsRegistry) {
		corsRegistry.addMapping("/**").allowedOrigins("*")
				.allowedMethods("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH", "TRACE", "OPTIONS").maxAge(3600);
	}

	@Override
	public void configureHttpMessageCodecs(ServerCodecConfigurer configurer) {
		configurer.registerDefaults(true);
		configurer.customCodecs().decoder(Jaxb2XmlDecoder());
		configurer.customCodecs().encoder(Jaxb2XmlEncoder());
	}

	@Bean
	public Decoder Jaxb2XmlDecoder() {
		return new OrgJson2XmlDecoder();
	}

	@Bean
	public Encoder Jaxb2XmlEncoder() {
		return new OrgJsonXmlEncoder();
	}

	@Bean
	public WebServerFactoryCustomizer<NettyReactiveWebServerFactory> serverFactoryCustomizer() {
		return new NettyTimeoutCustomizer();
	}

	@Bean
	public WebClient webClient() {
		return WebClient.builder().clientConnector(new ReactorClientHttpConnector(HttpClient.from(tcpClient())))
				.build();
	}

	@Bean

	public TcpClient tcpClient() {
		return TcpClient.create().option(ChannelOption.CONNECT_TIMEOUT_MILLIS, connectionTimeout).doOnConnected(connection -> {
			connection.addHandlerLast(new ReadTimeoutHandler(readtimeout, TimeUnit.MILLISECONDS));
			connection.addHandlerLast(new WriteTimeoutHandler(writetimeout, TimeUnit.MILLISECONDS));
		});
	}
}
