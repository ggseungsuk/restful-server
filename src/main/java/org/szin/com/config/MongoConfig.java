package org.szin.com.config;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;

import com.mongodb.MongoClientOptions;
import com.mongodb.reactivestreams.client.MongoClient;

import de.flapdoodle.embed.mongo.config.IMongoCmdOptions;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongoCmdOptionsBuilder;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.config.Storage;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;

@Configuration
public class MongoConfig {

	@Value("${spring.data.mongodb.storage:./flapdoodle/data}")
	String storage;

	@Value("${spring.data.mongodb.host:127.0.0.1}")
	String host;
	
	@Value("${spring.data.mongodb.port:27017}")
	int port;

	@Value("${spring.data.mongodb.database:restful}")
	String database;
	
	@Value("${spring.data.mongodb.min-connections-per-host:3}")
	int minConnectionsPerHost;
	
	@Value("${spring.data.mongodb.max-connections-per-host:200}")
	int maxConnectionsPerHost;
	
    @Autowired
    MongoClient mongoClient;
    
    
    /**

	@throws IOException 
     * @throws UnknownHostException 
     * @Bean
	public MongodExecutable mongodExecutable(){		
		MongodExecutable mongodExecutable = null;
		try {
		
	        IMongodConfig mongodConfig = new MongodConfigBuilder().version(Version.Main.PRODUCTION)
	            .net(new Net("localhost", 12767, Network.localhostIsIPv6()))
	            .build();
	 
	        MongodStarter starter = MongodStarter.getDefaultInstance();
	        mongodExecutable = starter.prepare(mongodConfig);
	        mongodExecutable.start();
	        
		}catch(Exception e) {
			e.printStackTrace();
		}
        return mongodExecutable;
	}
	*/

    
    @Bean
    public IMongoCmdOptions mongoCmdOptions() throws UnknownHostException, IOException {
    	IMongoCmdOptions cmdOptions = new MongoCmdOptionsBuilder().syncDelay(10).build();                
        return cmdOptions;
    }
    
    @Bean
    public IMongodConfig mongodConfig() throws UnknownHostException, IOException {
    	
    	if(new File(storage).exists() == false)
    		new File(storage).mkdirs();
    	
    	Storage replication = new Storage(new File(storage).getAbsolutePath(), null, 0);
        IMongodConfig mongodConfig = new MongodConfigBuilder().version(Version.Main.PRODUCTION)
        		.cmdOptions(new MongoCmdOptionsBuilder()
        				.syncDelay(10)
        				.build())
            .net(new Net(host, port, Network.localhostIsIPv6()))
            .replication(replication)
            .build();
                
        return mongodConfig;
    }
    
	@Bean
	public MongoClientOptions mongoClientOptions(){		
		return MongoClientOptions.builder()
				.minConnectionsPerHost(minConnectionsPerHost)
				.connectionsPerHost(maxConnectionsPerHost)
				.build();		
	}
 
    @Bean
    public ReactiveMongoTemplate reactiveMongoTemplate() {
        return new ReactiveMongoTemplate(mongoClient, database);
    }
}
