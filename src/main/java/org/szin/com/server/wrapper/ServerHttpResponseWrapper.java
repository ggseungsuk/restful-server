package org.szin.com.server.wrapper;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.util.MultiValueMap;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class ServerHttpResponseWrapper implements ServerHttpResponse{

	private ServerHttpResponse serverHttpResponse;
    private final StringBuilder cachedBody = new StringBuilder();
    private boolean cache = false;
    private List<Runnable> onCompletes = new ArrayList<Runnable>();
    private List<Consumer> onBeforeWriteWiths = new ArrayList<Consumer>();
    private boolean completed = false;
    
    public ServerHttpResponseWrapper(ServerHttpResponse serverHttpResponse) {
    	this.serverHttpResponse = serverHttpResponse;
	}
    
	public synchronized void enableCache() {
		if(this.cache == false) {
			this.cache = true;
		}
	}
	
    public void doOnCompleteBodyCache(Runnable onComplete) {
    	if(completed == true) {
    		onComplete.run();
    	}
    	onCompletes.add(onComplete);
    }
    
	public void doOnBeforeWriteWith(Consumer<? extends Publisher<? extends DataBuffer>> onBeforeWriteWith) {
		System.out.println("completed : " + completed);
    	if(completed != true) {
    		onBeforeWriteWiths.add(onBeforeWriteWith);
    	}
	}
	
	
    public void cache(DataBuffer buffer) {
        cachedBody.append(Charset.forName("UTF-8").decode(buffer.asByteBuffer()).toString());
    }

    public String getCachedBody() {
        return cachedBody.toString();
    }
    
	@Override
	public DataBufferFactory bufferFactory() {			
		return serverHttpResponse.bufferFactory();
	}

	@Override
	public void beforeCommit(Supplier<? extends Mono<Void>> action) {			
		serverHttpResponse.beforeCommit(action);
	}

	@Override
	public boolean isCommitted() {			
		return serverHttpResponse.isCommitted();
	}

	public void onBeforeWriteWiths(Publisher<?> body) {

		for(Consumer onBeforeWriteWith: onBeforeWriteWiths) {
			onBeforeWriteWith.accept(body);
		}
	}
	
	private Publisher<?> body;
		
	@Override
	public Mono<Void> writeWith(Publisher<? extends DataBuffer> body) {	
		onBeforeWriteWiths(body);
		if(cache)	
			body = toCacheBufs(Mono.from(body).doOnTerminate(this::onCompletes));
		return serverHttpResponse.writeWith(body);
	}	

	@Override
	public Mono<Void> writeAndFlushWith(Publisher<? extends Publisher<? extends DataBuffer>> body) {
		System.out.println("writeAndFlushWith :::");
		onBeforeWriteWiths(body);
		if(cache)	
			body = Flux.from(body).doOnNext(this::toCacheBufs).doOnComplete(this::onCompletes);
		return serverHttpResponse.writeAndFlushWith(body);
	}
	

	private Publisher<? extends DataBuffer> toCacheBufs(Publisher<? extends DataBuffer> body) {
		return body instanceof Mono ?
				Mono.from(body).doOnNext(this::cache)://.doAfterSuccessOrError((b,t)->{System.out.println(Charset.forName("UTF-8").decode(b.asByteBuffer()).toString());}) :
				Flux.from(body).doOnNext(this::cache);//.doOnError(e->{System.out.println("1111");});
	}

	private void onCompletes() {
		completed = true;
		for(Runnable onComplete: onCompletes) {
			onComplete.run();
		}
	}
	
	
	@Override
	public Mono<Void> setComplete() {			
		return serverHttpResponse.setComplete();
	}

	@Override
	public HttpHeaders getHeaders() {			
		return serverHttpResponse.getHeaders();
	}

	@Override
	public boolean setStatusCode(HttpStatus status) {			
		return serverHttpResponse.setStatusCode(status);
	}

	@Override
	public HttpStatus getStatusCode() {			
		return serverHttpResponse.getStatusCode();
	}

	@Override
	public MultiValueMap<String, ResponseCookie> getCookies() {			
		return serverHttpResponse.getCookies();
	}

	@Override
	public void addCookie(ResponseCookie cookie) {			
		serverHttpResponse.addCookie(cookie);
	}
}