package org.szin.com.server.wrapper;

import java.io.InputStream;
import java.io.SequenceInputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.charset.Charset;

import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.http.server.reactive.SslInfo;
import org.springframework.util.MultiValueMap;

import reactor.core.publisher.EmitterProcessor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxProcessor;
import reactor.core.publisher.Mono;

public class ServerHttpRequestWrapper implements ServerHttpRequest {

	private ServerHttpRequest serverHttpRequest;
	private Flux<DataBuffer> body;
	private boolean cache = false;
	private final StringBuilder cachedBody = new StringBuilder();

	public ServerHttpRequestWrapper(ServerHttpRequest serverHttpRequest) {
		this.serverHttpRequest = serverHttpRequest;
		
		FluxProcessor<DataBuffer, DataBuffer> source =
				EmitterProcessor.create();
		
		//source.reduce(new StringBuilder(), (a, b) -> a.append(Charset.forName("UTF-8").decode(b.asByteBuffer()).toString()))
		//	.subscribe(b -> System.out.println("here >> " + b.toString()));
		
		this.body = serverHttpRequest.getBody().doOnNext(source::onNext).doOnComplete(source::onComplete);
		
		
	}
	
	public InputStream newSequenceInputStream(InputStream i1, InputStream i2) {
		return new SequenceInputStream(i1, i2);
	}
	
	public static void main(String[] args) {
		
		Flux<Integer> mulFlux = Flux.just(1, 2, 3, 4);
		Mono<Integer> mulMono = mulFlux.log()
				.doOnNext(System.out::println)
				.doAfterTerminate(()->{System.out.println("doAfterTerminate");})
				.doOnCancel(()->{System.out.println("doOnCancel");})
				.doOnDiscard(Integer.class, System.out::println)
				.doOnEach(System.out::println)
				.reduce(new Integer(0), (acc, ele) -> { System.out.println("acc : " +acc + ", ele : " + ele ); return new Integer(acc.intValue() + ele.intValue()) ;});
		
		
		
		mulMono.log().subscribe(sum -> System.out.println("sum : " + sum));
	}

	public synchronized void enableCache() {
		if(this.cache == false) {
			this.body = this.body.doOnNext(this::cache);
						
			this.cache = true;
		}
	}

	private void cache(DataBuffer buffer) {

		cachedBody.append(Charset.forName("UTF-8").decode(buffer.asByteBuffer()).toString());
	}

	public void doOnCompleteBodyCache(Runnable onComplete) {
		this.body = this.body.doOnComplete(onComplete);
	}

	public String getCachedBody() {
		return cachedBody.toString();
	}

	@Override
	public String getMethodValue() {
		return serverHttpRequest.getMethodValue();
	}

	@Override
	public URI getURI() {
		return serverHttpRequest.getURI();
	}

	@Override
	public HttpHeaders getHeaders() {
		return serverHttpRequest.getHeaders();
	}

	@Override
	public Flux<DataBuffer> getBody() {
		return this.body;
	}

	@Override
	public String getId() {
		return serverHttpRequest.getId();
	}

	@Override
	public RequestPath getPath() {
		return serverHttpRequest.getPath();
	}

	@Override
	public MultiValueMap<String, String> getQueryParams() {
		return serverHttpRequest.getQueryParams();
	}

	@Override
	public MultiValueMap<String, HttpCookie> getCookies() {
		return serverHttpRequest.getCookies();
	}

	@Override
	public InetSocketAddress getRemoteAddress() {
		return serverHttpRequest.getRemoteAddress();
	}

	/**
	 * Return the SSL session information if the request has been transmitted over a
	 * secure protocol including SSL certificates, if available.
	 * 
	 * @return the session information, or {@code null} if none available
	 * @since 5.0.2
	 */
	@Override
	public SslInfo getSslInfo() {
		return serverHttpRequest.getSslInfo();
	}

	/**
	 * Return a builder to mutate properties of this request by wrapping it with
	 * {@link ServerHttpRequestDecorator} and returning either mutated values or
	 * delegating back to this instance.
	 */
	@Override
	public ServerHttpRequest.Builder mutate() {
		return serverHttpRequest.mutate();
	}

}