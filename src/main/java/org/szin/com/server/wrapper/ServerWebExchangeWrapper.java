package org.szin.com.server.wrapper;

import java.security.Principal;
import java.time.Instant;
import java.util.Map;
import java.util.function.Function;

import org.springframework.context.ApplicationContext;
import org.springframework.context.i18n.LocaleContext;
import org.springframework.http.codec.multipart.Part;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebSession;

import reactor.core.publisher.Mono;

public class ServerWebExchangeWrapper implements ServerWebExchange{

	private ServerWebExchange serverWebExchange;
	private ServerHttpRequestWrapper request;
	private ServerHttpResponseWrapper response;
	
	public ServerWebExchangeWrapper(ServerWebExchange serverWebExchange) {
		this.serverWebExchange = serverWebExchange;
		this.request = new ServerHttpRequestWrapper(serverWebExchange.getRequest());
		this.response = new ServerHttpResponseWrapper(serverWebExchange.getResponse());
	}
	
	public void enableCache() {
		enableRequestCache();
		enableResponseCache();
	}
	public void enableRequestCache() {
		this.request.enableCache();
	}
	public void enableResponseCache() {
		this.response.enableCache();
	}
	
	@Override
	public ServerHttpRequest getRequest() {			
		return request;
	}

	@Override
	public ServerHttpResponse getResponse() {			
		return this.response;
	}

	@Override
	public Map<String, Object> getAttributes() {			
		return serverWebExchange.getAttributes();
	}

	@Override
	public Mono<WebSession> getSession() {			
		return serverWebExchange.getSession();
	}

	@Override
	public <T extends Principal> Mono<T> getPrincipal() {			
		return serverWebExchange.getPrincipal();
	}

	@Override
	public Mono<MultiValueMap<String, String>> getFormData() {			
		return serverWebExchange.getFormData();
	}

	@Override
	public Mono<MultiValueMap<String, Part>> getMultipartData() {			
		return serverWebExchange.getMultipartData();
	}

	@Override
	public LocaleContext getLocaleContext() {			
		return serverWebExchange.getLocaleContext();
	}

	@Override
	public ApplicationContext getApplicationContext() {			
		return serverWebExchange.getApplicationContext();
	}

	@Override
	public boolean isNotModified() {			
		return serverWebExchange.isNotModified();
	}

	@Override
	public boolean checkNotModified(Instant lastModified) {			
		return serverWebExchange.checkNotModified(lastModified);
	}

	@Override
	public boolean checkNotModified(String etag) {			
		return serverWebExchange.checkNotModified(etag);
	}

	@Override
	public boolean checkNotModified(String etag, Instant lastModified) {			
		return serverWebExchange.checkNotModified(etag, lastModified);
	}

	@Override
	public String transformUrl(String url) {			
		return serverWebExchange.transformUrl(url);
	}

	@Override
	public void addUrlTransformer(Function<String, String> transformer) {			
		serverWebExchange.addUrlTransformer(transformer);
	}

	@Override
	public String getLogPrefix() {			
		return serverWebExchange.getLogPrefix();
	}

}