package org.szin.com.vo;

public class PageVo {

	private int page;
	private int countPerPage;
	
	public PageVo(int page, int countPerPage) {
		this.page = page;
		this.countPerPage = countPerPage;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getCountPerPage() {
		return countPerPage;
	}

	public void setCountPerPage(int countPerPage) {
		this.countPerPage = countPerPage;
	}

	@Override
	public String toString() {
		return "PageVo [page=" + page + ", countPerPage=" + countPerPage + "]";
	}
}
