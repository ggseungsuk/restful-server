package org.szin;

import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.reactive.config.EnableWebFlux;

import de.flapdoodle.embed.mongo.MongodExecutable;

@SpringBootApplication
@EnableWebFlux
public class RestfulServerApp {
	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = null;
		try {
			ctx = SpringApplication.run(RestfulServerApp.class, args);
		} catch (Throwable e) {
			while (true) {
				if (e.getMessage().equals("Could not start process: <EOF>")) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					ctx = SpringApplication.run(RestfulServerApp.class, args);
					break;
				}

				if (e.getCause() == null)
					break;
				e = e.getCause();
			}
		}
	}

	@Autowired
	ConfigurableApplicationContext context;

	@PreDestroy
	public void onExit() {
		/**
		if (context != null && context.getBean(MongodExecutable.class) != null) {
			MongodExecutable executable = (MongodExecutable) context.getBean(MongodExecutable.class);
			executable.stop();
		}
		*/
	}
}
