package org.szin.biz.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import org.szin.biz.service.TemplateService;
import org.szin.com.error.BadRequestException;
import org.szin.com.vo.PageVo;
import org.szin.util.StopPointUtil;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping
public class TemplateController {

	@Value("${http.header.resource-name-key:resource-name-key}")
	String resourceNameKey ;
	
	@Value("${resource.name-keys:resourcename,name,username,id}")
	List<String> resourceNameKeys ;

	@Value("${http.header.page-key:page}")
	String pageKey ;
	
	@Value("${http.header.count-per-page-key:count-per-page}")
	String countPerPageKey ;

	@Value("${paging.count-per-page:5}")
	int countPerPage ;

	@Value("${http.header.search-type-key:search-type}")
	String searchTypeKey ;

	@Value("${resource.name.expr:\\w@.-}")
	String resourceNameExpr ;
		
	
	private final static Logger logger = LoggerFactory.getLogger(TemplateController.class);	
	
	@Autowired
	TemplateService service;

	@RequestMapping(value = "/", method = RequestMethod.OPTIONS)
	public void optionsRoot(ServerHttpRequest request, ServerHttpResponse response) {			
		response.getHeaders().add("Allow", "GET");
	}
	
	@RequestMapping(value = "/**", method = RequestMethod.OPTIONS)
	public void options(ServerHttpRequest request, ServerHttpResponse response) {
		response.getHeaders().add("Allow", "GET,POST,PUT,DELETE,HEAD,OPTIONS,PATCH");
	}
		
	
	@RequestMapping(value = "/**", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Mono<Map> post(ServerHttpRequest request, @RequestBody Map<String, Object> body) {

		if(StopPointUtil.isStopPoint(request, "TemplateController.1")) return Mono.empty();
		
		if(request.getURI().getPath().contains("*")) {
			throw new BadRequestException("URI cannot contain *");
		}
		
		if(body.isEmpty()) {
			throw new BadRequestException("resource body cannot empty");
		}

		if(StopPointUtil.isStopPoint(request, "TemplateController.2")) return Mono.empty();
		
		String parentUri = request.getURI().getPath();
		if(parentUri.endsWith("/")) parentUri = parentUri.substring(0, parentUri.length() - 1);
		String uri = parentUri + "/" + getResourceName(request, body);
		uri = uri.replace("//", "/");

		if(StopPointUtil.isStopPoint(request, "TemplateController.3")) return Mono.empty();

		return service.post(parentUri, uri, body);
	}
	
	public String getResourceName(ServerHttpRequest request, Map<String, Object> body) {
		String resourceName = null;
		
		List<String> keys = request.getHeaders().get(resourceNameKey);
		if(keys == null) keys = new ArrayList<String>();
		keys.addAll(resourceNameKeys);
				
		for(String key : keys) {			
			if(body.containsKey(key)) { // key에 해당하는 이름이 있으면
				String resourceNameTemp = String.valueOf(body.get(key)).replaceAll("[^" + resourceNameExpr + "]+", "");
				if(!"".equals(resourceNameTemp)) {
					resourceName = resourceNameTemp;
					break;
				}
			}
			
			for(String mapKey : body.keySet()) { // ignore case sensitive
				if(key.equalsIgnoreCase(mapKey)) {
					if(body.get(mapKey) != null && !"".equals(body.get(mapKey))) {
						String resourceNameTemp = String.valueOf(body.get(mapKey)).replaceAll("[^" + resourceNameExpr + "]+", "");
						if(!"".equals(resourceNameTemp)) {
							resourceName = resourceNameTemp;
							break;
						}
					}
				}
			}
			if(resourceName != null) break;
		}
		
		if (resourceName == null)
			resourceName = UUID.randomUUID().toString();
		
		return resourceName;
	}

	@RequestMapping(value = "/**", method = RequestMethod.HEAD)
	public void head(ServerHttpRequest request, ServerHttpResponse response) {
		String resourcePath = request.getURI().getPath();
		if (resourcePath.endsWith("*")) {
			setPaging(request, response);
		}
	}
		
	@RequestMapping(value = "/**", method = RequestMethod.GET)
	public Publisher<Map> get(ServerHttpRequest request, ServerHttpResponse response) {

		String resourcePath = request.getURI().getPath();
		String expr = null;
		
		if (resourcePath.endsWith("/**")) {
			resourcePath = resourcePath.substring(0, resourcePath.lastIndexOf("/"));
			expr = "**";
		} else if (resourcePath.endsWith("/*")) {
			resourcePath = resourcePath.substring(0, resourcePath.lastIndexOf("/"));
			expr = "*";
		} else {
			expr = getUserParam(request, searchTypeKey, null);
		}
		
		if(expr == null) {
			return service.get(resourcePath);
		}else if(expr.equalsIgnoreCase("*")) {
			return service.list(setPaging(request, response), resourcePath, true);
		}else if(expr.equalsIgnoreCase("**")) {
			return service.list(setPaging(request, response), resourcePath, false);
		}else {
			if(expr != null || !"".equals(expr)) response.getHeaders().add("warning-msg", "search-type supports only * and **.");
			return service.get(resourcePath);
		}
	}

	@RequestMapping(value = "/**", method = RequestMethod.PUT)
	public Mono<Void> put(ServerHttpRequest request, @RequestBody Map<String, Object> body) {

		return service.put(request.getURI().getPath(), body);
	}

	@RequestMapping(value = "/**", method = RequestMethod.PATCH)
	public Mono<Void> patch(ServerHttpRequest request, @RequestBody Map<String, Object> body) {

		return service.put(request.getURI().getPath(), body);
	}

	@RequestMapping(value = "/**", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public Mono<Void> delete(ServerHttpRequest request) {

		return service.delete(request.getURI().getPath());
	}
	
	private PageVo setPaging(ServerHttpRequest request, ServerHttpResponse response) {

		int page = 0;
		int countPerPage = this.countPerPage;
		try {
			page = Math.max(1, Integer.parseInt(getUserParam(request, pageKey, "1")));
			response.getHeaders().set(pageKey, String.valueOf(page));
		}catch(Exception e) {
			throw new BadRequestException("invalid " + pageKey + " : " + getUserParam(request, pageKey, "") );
		}
		try {
			countPerPage = Math.max(Integer.parseInt(getUserParam(request, countPerPageKey, String.valueOf(this.countPerPage))), 1);
			response.getHeaders().set(countPerPageKey, String.valueOf(countPerPage));
		}catch(Exception e) {
			throw new BadRequestException("invalid " + countPerPageKey + " : " + getUserParam(request, countPerPageKey, "") );
		}	
		
		return new PageVo(page, countPerPage);
	}
	
	public String getUserParam(ServerHttpRequest request, String key, String def) {
		String param = getQueryParamFirst(request, key, null);
		if(param == null) param = getHeaderFirst(request, key, null);
		if(param == null) param = getQueryParamFirst(request, convertUnderscoreNameToPropertyName(key), null);
		if(param == null) param = getHeaderFirst(request, convertUnderscoreNameToPropertyName(key), def);
		
		return param;
	}

	public String getQueryParamFirst(ServerHttpRequest request, String key, String def) {
		return request.getQueryParams().toSingleValueMap().getOrDefault(key, def);
	}
	
	public String getHeaderFirst(ServerHttpRequest request, String key, String def) {
		List<String> header = request.getHeaders().get(key);
		if(header == null || header.size() == 0) return def;
		return header.get(0);
	}
	
	public static String convertUnderscoreNameToPropertyName(String name) {
		StringBuilder result = new StringBuilder();
		boolean nextIsUpper = false;
		if (name != null && name.length() > 0) {
			if (name.length() > 1 && name.substring(1, 2).equals("_")) {
				result.append(name.substring(0, 1).toUpperCase());
			}
			else {
				result.append(name.substring(0, 1).toLowerCase());
			}
			for (int i = 1; i < name.length(); i++) {
				String s = name.substring(i, i + 1);
				if (s.equals("_") || s.equals("-")) {
					nextIsUpper = true;
				}
				else {
					if (nextIsUpper) {
						result.append(s.toUpperCase());
						nextIsUpper = false;
					}
					else {
						result.append(s.toLowerCase());
					}
				}
			}
		}
		return result.toString();
	}
}
