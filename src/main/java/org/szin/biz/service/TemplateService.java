package org.szin.biz.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.index.CompoundIndexDefinition;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.szin.com.error.HttpException;
import org.szin.com.vo.PageVo;

import reactor.core.publisher.Mono;

@Service
public class TemplateService {

	private final static Logger logger = LoggerFactory.getLogger(TemplateService.class);

	@Autowired
	ReactiveMongoTemplate template;

	@Value("${spring.data.mongodb.collection:resources}")
	String collection;

	@Value("${spring.data.mongodb.exprie-after-seconds:3600}")
	int expireAfterSeconds;

	@Value("${resource.path-key:resourcePath}")
	String resourcePathKey;

	@Value("${resource.creation-time-key:creationTime}")
	String creationTimeKey;

	@Value("${resource.expiration-time-key:expirationTime}")
	String expirationTimeKey;

	@Value("${resource.relationship.use:false}")
	boolean useResourceRelationship;

	@Value("${xml.root.element.name.key:_xmlRootElementName}")
	String xmlRootElementNameKey ;	
	
	@PostConstruct
	public void init() {
		
		// MongoDB Index 생성
		Document indexOptions = new Document();
		indexOptions.put(resourcePathKey, -1);
		Index indexDefinition = new CompoundIndexDefinition(indexOptions).unique();

		template.indexOps(collection).ensureIndex(indexDefinition).doOnSuccess(s -> {
			logger.info(resourcePathKey + " index Created !");
		}).doOnError(t -> {
			logger.warn(t.getMessage());
		}).subscribe();
		
		template.indexOps(collection)
			.dropIndex(expirationTimeKey + "_-1")
			.doOnError(t -> {
				logger.warn(t.getMessage());
			}).subscribe();
		
		// resource 만료시각 인덱스 생성
		if (expireAfterSeconds > 0) {			
			Document expireIndexOptions = new Document();
			expireIndexOptions.put(expirationTimeKey, -1);
			Index expireIndexDefinition = new CompoundIndexDefinition(expireIndexOptions).expire(0,	TimeUnit.SECONDS);

			template.indexOps(collection).ensureIndex(expireIndexDefinition).doOnSuccess(s -> {
				logger.info(expirationTimeKey + " index Created !");
			}).doOnError(t -> {
				logger.warn(t.getMessage());
			}).subscribe();
			
			if(useResourceRelationship)
				updateDelayExprieTime();
		}
	}
	
	/**
	 * Resource 등록
	 * @param parentPath
	 * @param resourcePath
	 * @param vo
	 * @return
	 */
	public Mono<Map> post(String parentPath, String resourcePath, Map<String, Object> vo) {
		return (!useResourceRelationship || "".equals(parentPath) ?  Mono.just(true) : exists(parentPath))
				.flatMap(exist -> {
					if (!exist) { // 부모 path에 해당하는 resource가 없을 경우
						return Mono.error(
								new HttpException("parent [" + parentPath + "] is undefined " + resourcePathKey, HttpStatus.NOT_FOUND));
					}
		
					// relationship mode이면서 만료시간이 있으면 부모 resource의 만료시간을 연장한다.
					if (useResourceRelationship && expireAfterSeconds > 0) {
						addDelayResourcePath(parentPath);
					}		
					
					vo.put(creationTimeKey, new Date());
					if (expireAfterSeconds > 0) {
						vo.put(expirationTimeKey, new Date(System.currentTimeMillis() + (expireAfterSeconds * 1000)));
					}
					vo.put(resourcePathKey, resourcePath);
		
					return template.insert(vo, collection).doOnNext(row -> {
						row.remove("_id");

					}).doOnError(t -> {
						if (t instanceof DuplicateKeyException) {
							throw new HttpException(t.getMessage(), HttpStatus.CONFLICT);
						}
					});
				});
	}	
	
	/**
	 * Resource 존재 여부 확인 
	 * @param resourcePath
	 * @return
	 */
	public Mono<Boolean> exists(String resourcePath) {
		return template.exists(getResourcePathMatchQuery(resourcePath), collection);
	}

	/**
	 * Resource 조회
	 * @param resourcePath
	 * @return
	 */
	public Mono<Map> get(final String resourcePath) {
		Query query = getResourcePathMatchQuery(resourcePath);
		query.fields().exclude("_id");
		return template.findOne(query, Map.class, collection).switchIfEmpty(
				Mono.error(new HttpException(resourcePath + " is undefined " + resourcePathKey, HttpStatus.NOT_FOUND)));
	}

	/**
	 * Resource 목록 조회 
	 * @param page
	 * @param resourcePath
	 * @return
	 */
	public Mono<Map> list(PageVo page, String resourcePath) {
		return list(page, resourcePath, true);
	}

	/**
	 * Resource 목록 조회 
	 * @param page
	 * @param resourcePath
	 * @param childrenSearch : 직계 하위 요소만 조회 여부
	 * @return
	 */
	public Mono<Map> list(PageVo page, String resourcePath, boolean childrenSearch ) {
		Criteria criteria = Criteria.where(resourcePathKey);
		if (childrenSearch ) { // 직계 자식만 조회
			criteria.regex("(^" + resourcePath + "\\/)[^\\/]+$", "i");
		} else { // 모든 하위 리소스 조회
			criteria.regex("(^" + resourcePath + "\\/)+", "i");
		}

		final Query query = new Query(criteria);
		query.fields().exclude("_id");

		return template.count(query, collection).flatMap(totalcount -> {
			Map<Object, Object> pageResult = new HashMap<Object, Object>();
			pageResult.put(xmlRootElementNameKey, "resources");
			pageResult.put("totalcount", totalcount);
			pageResult.put("page", page.getPage());
			pageResult.put("countPerPage", page.getCountPerPage());
			if(totalcount == 0) return Mono.just(pageResult);
			
			return template.find(query.with(PageRequest.of(page.getPage()-1, page.getCountPerPage(), Sort.by(resourcePathKey))), Map.class, collection).reduce(new ArrayList<Map>(), (list,vo) -> {
				list.add(vo);
				return list;
			}).flatMap(list -> {
				pageResult.put("resource", list);
				return Mono.just(pageResult);
			});
		});
	}
		
	/**
	 * Resource 수정
	 * @param resourcePath
	 * @param vo
	 * @return
	 */
	public Mono<Void> put(String resourcePath, Map<String, Object> vo) {
		Update update = new Update();
		for (String key : vo.keySet()) {
			if (resourcePathKey.equals(key) || expirationTimeKey.equals(key) || creationTimeKey.equals(key)) {
				continue;
			}
			update.set(key, vo.get(key));
		}

		return template.updateFirst(getResourcePathMatchQuery(resourcePath), update, Map.class, collection)
				.flatMap(r -> {
					if (r.getMatchedCount() == 0)
						return Mono.error(new HttpException(resourcePath + " is undefined " + resourcePathKey,
								HttpStatus.NOT_FOUND));
					return Mono.empty();
				});
	}

	/**
	 * Resource 삭제
	 * @param resourcePath
	 * @return
	 */
	public Mono<Void> delete(String resourcePath) {
		Query query = null;
		if(useResourceRelationship) {  // relationship mode의 경우 자식 resource도 삭제
			Criteria criteria = new Criteria();
			criteria.orOperator(Criteria.where(resourcePathKey).is(resourcePath), Criteria.where(resourcePathKey).regex("(^" + resourcePath + "\\/)+", "i"));
			
			query = new Query(criteria);
		}else {
			query = getResourcePathMatchQuery(resourcePath);
		}
		
		return template.remove(query, collection).flatMap(r -> {
			if (r.getDeletedCount() == 0)
				return Mono.error(
						new HttpException(resourcePath + " is undefined " + resourcePathKey, HttpStatus.NOT_FOUND));
			return Mono.empty();
		});
	}

	/**
	 * resourcePath용 기본 Query를 돌려준다.
	 * @param resourcePath
	 * @return
	 */
	public Query getResourcePathMatchQuery(String resourcePath) {
		Criteria criteria = new Criteria(resourcePathKey);
		criteria.is(resourcePath);
		Query query = new Query(criteria);
		return query;
	}
	
	
	/**
	 * relationship mode면서 expire 시간이 있을경우 자식 등록 시 부모의 exprie 시간을 같이 늘여준다.
	 */
	// resourcePaths
	Set<String> delayResourcePaths = new HashSet<String>();

	public Set<String> getDelayResourcePathSet(){
		return getDelayResourcePathSet(false);
	}
	
	/**
	 * delayResourcePaths set에 추가 할때와 set을 DB로 저장할 때 동기화 시키기 위해 필요
	 * @param cloneAndInit
	 * @return
	 */
	public synchronized Set<String> getDelayResourcePathSet(boolean cloneAndInit){
		if(cloneAndInit) {
			Set<String> clone = new HashSet<String>(delayResourcePaths);
			delayResourcePaths.clear();
			return clone;
		}
		return delayResourcePaths;
	}
	
	/**
	 * 연장 할 resourcePath 추가
	 * @param parentPath
	 */
	public void addDelayResourcePath(String parentPath) {

		String[] paths = parentPath.split("/");
		String path = "";
		for(String nextPath : paths) {
			if(nextPath.equals("")) continue;
			path += "/" + nextPath;
			getDelayResourcePathSet().add(path);
		}
	}
	
	/**
	 * DB에 적용
	 */
	public void updateDelayExprieTime() {
		
		new Thread(() -> {
		
			while(true) {
				try {
					Set<String> delayResourcePaths = getDelayResourcePathSet(true);
		
					if(delayResourcePaths.size() > 0) {					
						Update update = new Update();
						update.set(expirationTimeKey, new Date(System.currentTimeMillis() + (expireAfterSeconds * 1000)));
						
						Criteria criteria = new Criteria(resourcePathKey);
						criteria.in(delayResourcePaths);
						Query query = new Query(criteria);
									
						template.updateMulti(query, update, Map.class, collection).subscribe();	
					}
		
					Thread.sleep(100);
				}catch(Exception e) {
					e.printStackTrace();
				}		
			}
		}).start();
	}
}
