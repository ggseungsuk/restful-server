package org.szin.util;

import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.json.JSONObject;
import org.json.XML;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ResolvableType;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.codec.xml.Jaxb2XmlDecoder;
import org.springframework.http.codec.xml.XmlEventDecoder;
import org.springframework.lang.Nullable;
import org.springframework.util.MimeType;
import org.springframework.util.xml.StaxUtils;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class OrgJson2XmlDecoder extends Jaxb2XmlDecoder{

	
	@Value("${xml.root.element.name.key:_xmlRootElementName}")
	String xmlRootElementNameKey ;
	
	private final XmlEventDecoder xmlEventDecoder = new XmlEventDecoder();
	
	@Override
	public boolean canDecode(ResolvableType elementType, @Nullable MimeType mimeType) {
		
		boolean canDecode = false;
		for (MimeType candidate : getDecodableMimeTypes()) {
			if (candidate.isCompatibleWith(mimeType)) {
				canDecode = true;
			}
		}
		if(canDecode) {
			Class<?> outputClass = elementType.toClass();			
			return Map.class.isAssignableFrom(outputClass ) || super.canDecode(elementType, mimeType);
		}
		return false;
	}
	

	@Override
	public Mono<Object> decodeToMono(Publisher<DataBuffer> inputStream, ResolvableType elementType,
			@Nullable MimeType mimeType, @Nullable Map<String, Object> hints) {
		
		if(!elementType.toClass().isAnnotationPresent(XmlRootElement.class) && elementType.toClass().isAssignableFrom( Map.class)){
			return decode(inputStream, elementType, mimeType, hints).singleOrEmpty();			
		}else {		
			return super.decode(inputStream, elementType, mimeType, hints).singleOrEmpty();
		}
	}


	@Override
	public Flux<Object> decode(Publisher<DataBuffer> inputStream, ResolvableType elementType,
			@Nullable MimeType mimeType, @Nullable Map<String, Object> hints) {
				
		Flux<DataBuffer> mono = Flux.from(DataBufferUtils.join(inputStream));
		return mono.map(db -> {
			InputStreamReader xml = new InputStreamReader(db.asInputStream());
			JSONObject xmlJSONObj = XML.toJSONObject(xml);
			Map<String, Object> result = xmlJSONObj.toMap();
			
			if(result.keySet().size() == 1) {
				String rootName = (String)result.keySet().toArray()[0];
				if(result.get(rootName) instanceof Map){
					result = (Map<String, Object>)result.remove(rootName);
					result.put(xmlRootElementNameKey, rootName);
				}
			}
			
			return result;
		});
	}

	public Flux<Object> old_decode(Publisher<DataBuffer> inputStream, ResolvableType elementType,
			@Nullable MimeType mimeType, @Nullable Map<String, Object> hints) {

		Flux<XMLEvent> xmlEventFlux = this.xmlEventDecoder.decode(
				inputStream, ResolvableType.forClass(XMLEvent.class), mimeType, hints);

		Flux<List<XMLEvent>> splitEvents = Flux.from(xmlEventFlux.collectList());
		
		return splitEvents.map(events -> {
			XMLEventReader eventReader = StaxUtils.createXMLEventReader(events);
			Map<String, Object> value = new HashMap<String, Object>();
			try {
				value = unmarshal(eventReader, value);
				for(String key : value.keySet()) {
					if(value.get(key) instanceof Map) {
						value = (Map<String, Object>)value.get(key);
						value.put(xmlRootElementNameKey, key);
					}
				}
			} catch (XMLStreamException e) {
				e.printStackTrace();
			}
			
			return value;
		});
	}
	
	

	public Map<String, Object> unmarshal(XMLEventReader eventReader, Map<String, Object> value) throws XMLStreamException {
		
		String key = null;
		Object val = null;	
		
		while(eventReader.hasNext()) {
			  XMLEvent event = eventReader.nextEvent();

			  if(event.isStartElement()) {
				  if(key != null) {
					  Map<String, Object> childMap = new HashMap<String, Object>();
					  childMap.put("_parent", value);
					  value.put(key, childMap);
					  value = childMap;
				  }
				  
				  key = event.asStartElement().getName().toString();
				  val = null;
			  } else if(event.isEndElement()) {
		  	      if(key == null) {
		  	    	  value = (Map<String, Object>)value.remove("_parent");
		  	      }else {
		  	    	  value.put(key, val);
		  	      }
			  	  key = null;
			  	  val = null;
			  } else if(event.isCharacters()) {
			  	  val = event.asCharacters().getData();
			  }
		}
		
		
		return value;
	}	
}
