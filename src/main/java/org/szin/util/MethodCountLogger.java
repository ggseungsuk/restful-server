package org.szin.util;

import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MethodCountLogger {

	private final static Logger logger = LoggerFactory.getLogger(MethodCountLogger.class);

	public MethodCountLogger() {

		if (logger.isDebugEnabled()) {
			new Thread() {
				public void run() {
					while (true) {
						try {
							Thread.sleep(5 * 1000);
						} catch (InterruptedException e) {
							logger.error(e.getMessage(), e);
						}
						
						if(totalCnt.get() != postCnt.get() + getCnt.get()  + putCnt.get() + deleteCnt.get()) {
							totalCnt.set( postCnt.get() + getCnt.get()  + putCnt.get() + deleteCnt.get() );
							logger.error("API Call Count >> post : " + postCnt.get() + ", get : " + getCnt.get() + ", put : "
									+ putCnt.get() + ", delete : " + deleteCnt.get());
						}
					}
				}
			}.start();
		}
	}

	AtomicLong totalCnt = new AtomicLong();
	AtomicLong postCnt = new AtomicLong();
	AtomicLong getCnt = new AtomicLong();
	AtomicLong putCnt = new AtomicLong();
	AtomicLong deleteCnt = new AtomicLong();

	public synchronized void addPost() {
		postCnt.getAndIncrement();
	}

	public synchronized void addGet() {
		postCnt.getAndIncrement();
	}

	public synchronized void addPut() {
		postCnt.getAndIncrement();
	}

	public synchronized void addDelete() {
		postCnt.getAndIncrement();
	}

}
