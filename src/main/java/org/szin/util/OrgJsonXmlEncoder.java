package org.szin.util;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.json.JSONObject;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ResolvableType;
import org.springframework.core.codec.AbstractEncoder;
import org.springframework.core.codec.EncodingException;
import org.springframework.core.codec.Hints;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.log.LogFormatUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class OrgJsonXmlEncoder extends AbstractEncoder<Object>{

	
	@Value("${xml.root.element.name.key:_xmlRootElementName}")
	String xmlRootElementNameKey ;
	
	final static ObjectMapper om = new ObjectMapper();

	public OrgJsonXmlEncoder() {
		super(MimeTypeUtils.APPLICATION_XML, MimeTypeUtils.TEXT_XML);
	}
	
	@Override
	public boolean canEncode(ResolvableType elementType, @Nullable MimeType mimeType) {
		Class<?> outputClass = elementType.toClass();
		
		if (super.canEncode(elementType, mimeType)) {
			return Map.class.isAssignableFrom( outputClass) || (outputClass.isAnnotationPresent(XmlRootElement.class) ||
					outputClass.isAnnotationPresent(XmlType.class));
		}
		
		return false;
	}


	@Override
	public final Flux<DataBuffer> encode(Publisher<? extends Object> inputStream, DataBufferFactory bufferFactory,
			ResolvableType elementType, @Nullable MimeType mimeType, @Nullable Map<String, Object> hints) {

		return Flux.from(inputStream)
				.concatMap(new Function<Object, Publisher<DataBuffer>>() {
					boolean isFirst = true;					
					@Override
					public Publisher<DataBuffer> apply(Object value) {
						Flux<DataBuffer> result = encodeMap(isFirst,  (Map<String, Object>)value, bufferFactory, elementType, mimeType, hints);
						isFirst  = false;
						return result;
					}					
				});
	}
	
	protected Flux<DataBuffer> encodeMap(boolean isFirst, Map<String, Object> value, DataBufferFactory bufferFactory,
			ResolvableType type, @Nullable MimeType mimeType, @Nullable Map<String, Object> hints) {

		if (!Hints.isLoggingSuppressed(hints)) {
			LogFormatUtils.traceDebug(logger, traceOn -> {
				String formatted = LogFormatUtils.formatValue(value, !traceOn);
				return Hints.getLogPrefix(hints) + "Encoding [" + formatted + "]";
			});
		}
		
		return Mono.fromCallable(() -> {
			boolean release = true;
			DataBuffer buffer = bufferFactory.allocateBuffer(1024);
			try {
				OutputStream outputStream = buffer.asOutputStream();
				
				Map<String, Object> root = new HashMap<String, Object>();

				String rootName = null;
				XmlRootElement xre = value.getClass().getAnnotation(XmlRootElement.class);
				if(xre != null) {
					rootName = StringUtil.empty(xre.name(), value.getClass().getSimpleName());
				}
				if(rootName == null && value.get(xmlRootElementNameKey) != null && !"".equals(value.get(xmlRootElementNameKey))) {
					rootName = String.valueOf(value.remove(xmlRootElementNameKey));
				}

				if(rootName == null) rootName = "resource";
				
				root.put(rootName, value);

				String xml = XMLImpl.toString(new JSONObject(om.writeValueAsString(root)), xmlRootElementNameKey);
				
				if(isFirst)
					outputStream.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>".getBytes("UTF-8"));
				
				outputStream.write(xml.getBytes("UTF-8"));
				
				release = false;
				return buffer;  // relying on doOnDiscard in base class
			}
			catch (Exception ex) {
				throw new EncodingException(
						"Could not marshal " + value.getClass() + " to XML", ex);
			}
			finally {
				if (release) {
					DataBufferUtils.release(buffer);
				}
			}
		}).flux();
	}

	
	protected Flux<DataBuffer> encode(Object value, DataBufferFactory bufferFactory,
			ResolvableType type, @Nullable MimeType mimeType, @Nullable Map<String, Object> hints) {

		
		//if(!type.toClass().isAnnotationPresent(XmlRootElement.class) && type.toClass().isAssignableFrom( Map.class)){
			return encodeMap(true, (Map<String, Object>)value, bufferFactory, type, mimeType, hints);
		//}else {
		//	return super.encode(value, bufferFactory, type, mimeType, hints);
		//}
	}
	
}
