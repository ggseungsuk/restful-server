package org.szin.util;

import java.util.Map;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;

public class StopPointUtil {

	private static String STOP_POINT_KEY = "stop-point";
	
	public static boolean isStopPoint(ServerWebExchange serverWebExchange, String pointName) {

		return isStopPoint(serverWebExchange.getRequest(), pointName);
	}
	

	public static boolean isStopPoint(ServerHttpRequest request, String pointName) {

		return isStopPoint(request.getHeaders().toSingleValueMap(), pointName);
	}

	public static boolean isStopPoint(Map<String, String> map, String pointName) {
		if(pointName == null) return false;
		
		return (pointName.equals(map.get(STOP_POINT_KEY)));
	}
}
