package org.szin.util;

import java.nio.charset.Charset;
import java.util.Locale;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;


@Component
public class StringUtil {

	private static MessageSource messageSource;

	@Autowired
	private static Environment env;

	@Autowired(required=true)
	public void setMessageSource(MessageSource messageSource) {
		StringUtil.messageSource = messageSource;
	}
	
	public static MessageSource getMessageSource() {
		return messageSource;
	}
	
	/**
	 * 입력된 코드를 메세지로 변환한다.
	 * @param cd
	 * @return
	 */
	public static String code2message(String cd){
		return code2message(cd , null);
	}


	/**
	 * 입력된 코드와 문자를 메세지로 변환한다.
	 * @param cd
	 * @return
	 */
	public static String code2message(String cd , Object[] params){
		if(params == null){
			params = new String[]{};
		}
		Locale lo = Locale.KOREAN;//CommonUtil.getLocale();
		Object[] newParam = new Object[params.length];
		for(int i=0;i<params.length; i++){
			try {
				newParam[i] = messageSource.getMessage((String) params[i], null, lo);
			} catch (NoSuchMessageException e) {
				newParam[i] = params[i];
			}
		}
		if(cd == null || "".equals(cd.trim())) return null;
		return messageSource.getMessage(cd, newParam , lo);
	}

	/**
	 * empty
	 * @param v
	 * @param d
	 * @return
	 */
	public static String empty(String v , String d){
		return isEmpty(v)?d:v;
	}

	/**
	 * empty
	 * @param v
	 * @param d
	 * @return
	 */
	public static boolean isEmpty(String v){
		return (v==null || "".equals(v));
	}

	public static boolean isYes(String str){
		if(str == null) return false;
		return "true".equalsIgnoreCase(str) || "1".equals(str) || "y".equalsIgnoreCase(str) || "yes".equalsIgnoreCase(str) || "ok".equalsIgnoreCase(str);
	}
	
	public static String getRandomString(int length) {
	    byte[] array = new byte[length];
	    new Random().nextBytes(array);
	    return new String(array, Charset.forName("UTF-8"));
	 
	}
}
