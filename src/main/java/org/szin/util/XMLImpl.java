package org.szin.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.json.XMLParserConfiguration;

public class XMLImpl{

    public static String toString(Object object, String xmlRootElementName) throws JSONException {
        return toString(object, null, XMLParserConfiguration.ORIGINAL, xmlRootElementName);
    }
	
    public static String toString(final Object object, final String tagName, final XMLParserConfiguration config, String xmlRootElementTagName)
            throws JSONException {
        StringBuilder sb = new StringBuilder();
        JSONArray ja;
        JSONObject jo;
        String string;
        String attrTagName = tagName;

        if (object instanceof JSONObject) {
        	
        	if(((JSONObject)object).has(xmlRootElementTagName)) {

            	String rootElementName = ((JSONObject)object).getString(xmlRootElementTagName);
        		if(rootElementName != null && !"".equals(rootElementName)) {
        			attrTagName = rootElementName;
        			((JSONObject)object).remove(xmlRootElementTagName);
        		}
        	}
        	
        	
            // Emit <tagName>
            if (tagName != null) {
                sb.append('<');
                sb.append(attrTagName);
                sb.append('>');
            }

            // Loop thru the keys.
            // don't use the new entrySet accessor to maintain Android Support
            jo = (JSONObject) object;
            for (final String key : jo.keySet()) {
                Object value = jo.opt(key);
                if (value == null) {
                    value = "";
                } else if (value.getClass().isArray()) {
                    value = new JSONArray(value);
                }

                // Emit content in body
                if (key.equals(config.cDataTagName)) {
                    if (value instanceof JSONArray) {
                        ja = (JSONArray) value;
                        int jaLength = ja.length();
                        // don't use the new iterator API to maintain support for Android
						for (int i = 0; i < jaLength; i++) {
                            if (i > 0) {
                                sb.append('\n');
                            }
                            Object val = ja.opt(i);
                            sb.append(XML.escape(val.toString()));
                        }
                    } else {
                        sb.append(XML.escape(value.toString()));
                    }

                    // Emit an array of similar keys

                } else if (value instanceof JSONArray) {
                    ja = (JSONArray) value;
                    int jaLength = ja.length();
                    // don't use the new iterator API to maintain support for Android
					for (int i = 0; i < jaLength; i++) {
                        Object val = ja.opt(i);
                        if (val instanceof JSONArray) {
                            sb.append('<');
                            sb.append(key);
                            sb.append('>');
                            sb.append(toString(val, null, config, xmlRootElementTagName));
                            sb.append("</");
                            sb.append(key);
                            sb.append('>');
                        } else {
                            sb.append(toString(val, key, config, xmlRootElementTagName));
                        }
                    }
                } else if ("".equals(value)) {
                    sb.append('<');
                    sb.append(key);
                    sb.append("/>");

                    // Emit a new tag <k>

                } else {
                    sb.append(toString(value, key, config, xmlRootElementTagName));
                }
            }
            if (attrTagName != null) {

                // Emit the </tagname> close tag
                sb.append("</");
                sb.append(attrTagName);
                sb.append('>');
            }
            return sb.toString();

        }

        if (object != null && (object instanceof JSONArray ||  object.getClass().isArray())) {
            if(object.getClass().isArray()) {
                ja = new JSONArray(object);
            } else {
                ja = (JSONArray) object;
            }
            int jaLength = ja.length();
            // don't use the new iterator API to maintain support for Android
			for (int i = 0; i < jaLength; i++) {
                Object val = ja.opt(i);
                // XML does not have good support for arrays. If an array
                // appears in a place where XML is lacking, synthesize an
                // <array> element.
                sb.append(toString(val, tagName == null ? "array" : tagName, config, xmlRootElementTagName));
            }
            return sb.toString();
        }

        string = (object == null) ? "null" : XML.escape(object.toString());
        return (tagName == null) ? "\"" + string + "\""
                : (string.length() == 0) ? "<" + tagName + "/>" : "<" + tagName
                        + ">" + string + "</" + tagName + ">";

    }
}
