package org.szin.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat(DateUtil.DATE_TIME_FORMAT_STRING);
	private static SimpleDateFormat dateTimeFormatSpace = new SimpleDateFormat(DateUtil.DATE_TIME_FORMAT_STRING_WITH_SPACE);
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
	public static final String DATE_TIME_FORMAT_STRING = "yyyyMMddHHmmss";
	public static final String DATE_TIME_FORMAT_STRING_WITH_SPACE = "yyyy-MM-dd HH:mm:ss";
	  
	public static String getTime(){
		return timeFormat.format(new Date());
	}
	  
	public static String getDate(){
		return dateFormat.format(new Date());
	}
	  
	public static String getDateTime(){
		return dateTimeFormat.format(new Date());
	}
	
	public static String getNow(){
		String now = "";
		
		SimpleDateFormat simpleFomat = new SimpleDateFormat(DATE_TIME_FORMAT_STRING);
		Date currentTime = new Date();
		now = simpleFomat.format(currentTime);
		
		return now;
	}
	
	public static String getNowForamt(){
		String now = "";
		
		Date currentTime = new Date();
		now = dateTimeFormatSpace.format(currentTime);
		
		return now;
	}
	
	public static String getNow(String format){
		String now = "";
		
		SimpleDateFormat simpleFomat = new SimpleDateFormat(format);
		Date currentTime = new Date();
		now = simpleFomat.format(currentTime);
		
		return now;
	}
	  
	public static String getDateTime(Date date){
		String now = "";
		
		SimpleDateFormat simpleFomat = new SimpleDateFormat(DATE_TIME_FORMAT_STRING);
		now = simpleFomat.format(date);
		
		return now;
	}
	
	public static String getDateTime(Date date, int intervalType, int interval){
		String now = "";
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(intervalType, interval);
	
		SimpleDateFormat simpleFomat = new SimpleDateFormat(DATE_TIME_FORMAT_STRING);
		now = simpleFomat.format(calendar.getTime());
		
		return now;
	}
	
	
	
	public static String getDateTime(Date date, String format){
		String now = "";
		
		SimpleDateFormat simpleFomat = new SimpleDateFormat(format);
		now = simpleFomat.format(date);
		
		return now;
	}
	
	public static long getDateTime(String date, String format) {
		try {
			SimpleDateFormat simpleFomat = new SimpleDateFormat(format);
			return simpleFomat.parse(date).getTime();
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}
	  
}
