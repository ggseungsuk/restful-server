#!/bin/bash

if [ -n "$RESTFUL_XMX" ]; then
	JAVA_OPTS="-Xmx$RESTFUL_XMX"
else
	JAVA_OPTS="-Xmx256m"
fi
JAVA_OPTS=$JAVA_OPTS" -Dserver.port=$RESTFUL_PORT"
JAVA_OPTS=$JAVA_OPTS" -Dos.arch=x86_64"
## If external mongodb is used 
#JAVA_OPTS=$JAVA_OPTS" -Dspring.active.profiles=external-mongo -Dspring.data.mongodb.host=  -Dspring.data.mongodb.port=  -Dspring.data.mongodb.username=  -Dspring.data.mongodb.password= "

exec java ${JAVA_OPTS} -jar /app/target/restful-server/restful-server-1.0.jar